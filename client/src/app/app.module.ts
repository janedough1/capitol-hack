import { SearchService } from './search.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { SetlistComponent } from './setlist/setlist.component';
import { SearchComponent } from './search/search.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { RouteRoutingModule } from './route/route-routing.module';
import { RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    SetlistComponent,
    SearchComponent,
    ScheduleComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouteRoutingModule,
    RouterModule.forRoot([], {useHash: true}),
    BrowserAnimationsModule
    
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
