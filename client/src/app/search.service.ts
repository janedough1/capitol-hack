import { API } from './api';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {

  constructor(private http: HttpClient) { }

  searchArtist(name): Observable<any> {
    return this.http
    .get(API.artistSearchURL);
  }

  searchTrack(name): Observable<any> {
    // return this.http
    // .get(API.trackSearchURL)
    // .map((res: any) => res.json())
    return Observable.of([
      {
        'name' : 'Some Kind Of Blue',
        'album' : 'RAT'
      }
    ])
  }

}
