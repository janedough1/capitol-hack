import { SearchService } from './../search.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';


@Component({
  selector: 'setlist',
  templateUrl: './setlist.component.html',
  styleUrls: ['./setlist.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class SetlistComponent implements OnInit {

  searchOpen = false;
  chooseSearch = false;
  artistName : String;
  catalog;;
  extraTracks = [];

  constructor(private search: SearchService, private router: Router) {
  }

  ngOnInit() {
   
  }

  openSearch() {
    this.searchOpen = true;
    this.search.searchArtist('mock').subscribe(results => {
      this.catalog = results.albums;
    });
  }

  openSearchOptions() {
    this.chooseSearch = !this.chooseSearch;
  }

  closeSearch() {
    this.chooseSearch = false;
  }

  closeSearchContainer() {
    this.searchOpen = false;
  }

  searchArtist() {
    this.search.searchArtist(this.artistName);
  }

  backToSchedule() {
    this.router.navigateByUrl('');
  }

  addTrack(track) {
    track.selected = !track.selected;
    if (track.selected) {
      this.extraTracks.push(track);
    }
  }

  addTracks() {
    this.searchOpen = false;
    this.chooseSearch = false;
  }

}
