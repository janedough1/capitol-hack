CREATE TABLE setlist (
    id int AUTO_INCREMENT NOT NULL,
    setname varchar(255),
    song varchar(255),
    primary key(id)
);

INSERT INTO setlist (id, setname, song)
VALUES ('1', 'set name', 'CapRec');