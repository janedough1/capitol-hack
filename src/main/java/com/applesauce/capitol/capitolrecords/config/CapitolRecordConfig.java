package com.applesauce.capitol.capitolrecords.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jan on 6/2/18.
 */
@Configuration
@ConfigurationProperties(prefix = "capitol")
public class CapitolRecordConfig {
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
