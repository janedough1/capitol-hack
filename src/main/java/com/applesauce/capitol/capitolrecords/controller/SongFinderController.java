package com.applesauce.capitol.capitolrecords.controller;

import com.applesauce.capitol.capitolrecords.model.artist.CapitolRecordArtist;
import com.applesauce.capitol.capitolrecords.model.release.CapitolRecordRelease;
import com.applesauce.capitol.capitolrecords.model.release.Release;
import com.applesauce.capitol.capitolrecords.model.response.Album;
import com.applesauce.capitol.capitolrecords.model.response.Artist;
import com.applesauce.capitol.capitolrecords.model.response.Song;
import com.applesauce.capitol.capitolrecords.model.setlist.Setlist;
import com.applesauce.capitol.capitolrecords.service.CapitolRecordsService;
import com.applesauce.capitol.capitolrecords.service.SetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Created by jan on 6/2/18.
 */

@RestController
public class SongFinderController {

    private final CapitolRecordsService capitolRecordsService;

    private final SetListService setListService;

    @Autowired
    public SongFinderController(CapitolRecordsService capitolRecordsService, SetListService setListService) {
        this.capitolRecordsService = capitolRecordsService;
        this.setListService = setListService;
    }

    @RequestMapping("/v1/getSongList/{name}")
    public Artist getArtistSongs(@PathVariable("name") String name) throws Exception{

        try {

            CapitolRecordArtist capitolRecordArtist = capitolRecordsService.searchArtist(name);

            com.applesauce.capitol.capitolrecords.model.artist.Artist foundArtist = capitolRecordArtist.getSearchResults().getSearchResult().get(0).getArtist();

            CapitolRecordRelease releases = capitolRecordsService.getReleases(foundArtist.getId());

            Map<String, String> albumMap =
                    releases.getReleases().getRelease().stream().collect(Collectors.toMap(Release::getId,
                            Release::getTitle));

            Artist artist = new Artist();
            artist.setName(foundArtist.getName());
            List<Album> albums = new ArrayList<>();
            for(Map.Entry<String, String> albumEntry : albumMap.entrySet()) {
                Album album = new Album();
                album.setAlbum(albumEntry.getValue());
                List<Song> songNames = capitolRecordsService.getAllSongs(albumEntry.getKey());
                album.setSongs(songNames);
                albums.add(album);
            }
            artist.setAlbums(albums);

            return artist;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @RequestMapping("/v1/getSetList")
    public List<Setlist> getSetlist() throws Exception{

        try {

            List<Setlist> setlists = setListService.getSetList();

            return setlists;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @RequestMapping("/v1/getSetList/{setname}")
    public List<Setlist> getSetlist(@PathVariable("setname") String setname) throws Exception{

        try {

            List<Setlist> setlists = setListService.getSetListBySetname(setname);

            return setlists;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @RequestMapping(value = "/v1/createSetList", method = RequestMethod.POST)
    public Setlist update(@RequestBody Setlist setlist) throws Exception {

        try {
            return setListService.createSetList(setlist);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @RequestMapping(value = "/v1/createSetLists", method = RequestMethod.POST)
    public List<Setlist> update(@RequestBody List<Setlist> setlists) throws Exception {

        try {
            return setListService.createSetListList(setlists);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
}