package com.applesauce.capitol.capitolrecords.exception;

/**
 * Created by jan on 6/2/18.
 */
public class CapitolRecordsException extends Exception {
    public CapitolRecordsException(String message, Throwable cause) {
        super(message, cause);
    }
}