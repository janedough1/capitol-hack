package com.applesauce.capitol.capitolrecords.exception;

/**
 * Created by jan on 6/3/18.
 */
public class SetlistException extends Exception {
    public SetlistException(String message, Throwable cause) {
        super(message, cause);
    }
}
