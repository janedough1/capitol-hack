
package com.applesauce.capitol.capitolrecords.model.artist;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "page",
    "pageSize",
    "totalItems",
    "searchResult"
})
public class SearchResults {

    @JsonProperty("page")
    private String page;
    @JsonProperty("pageSize")
    private String pageSize;
    @JsonProperty("totalItems")
    private String totalItems;
    @JsonProperty("searchResult")
    private List<SearchResult> searchResult = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("page")
    public String getPage() {
        return page;
    }

    @JsonProperty("page")
    public void setPage(String page) {
        this.page = page;
    }

    @JsonProperty("pageSize")
    public String getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("totalItems")
    public String getTotalItems() {
        return totalItems;
    }

    @JsonProperty("totalItems")
    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    @JsonProperty("searchResult")
    public List<SearchResult> getSearchResult() {
        return searchResult;
    }

    @JsonProperty("searchResult")
    public void setSearchResult(List<SearchResult> searchResult) {
        this.searchResult = searchResult;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
