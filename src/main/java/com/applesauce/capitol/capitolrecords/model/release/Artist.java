
package com.applesauce.capitol.capitolrecords.model.release;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "appearsAs",
    "image",
    "slug",
    "isPlaceholderImage"
})
public class Artist {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("appearsAs")
    private String appearsAs;
    @JsonProperty("image")
    private String image;
    @JsonProperty("slug")
    private String slug;
    @JsonProperty("isPlaceholderImage")
    private String isPlaceholderImage;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("appearsAs")
    public String getAppearsAs() {
        return appearsAs;
    }

    @JsonProperty("appearsAs")
    public void setAppearsAs(String appearsAs) {
        this.appearsAs = appearsAs;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    @JsonProperty("slug")
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("isPlaceholderImage")
    public String getIsPlaceholderImage() {
        return isPlaceholderImage;
    }

    @JsonProperty("isPlaceholderImage")
    public void setIsPlaceholderImage(String isPlaceholderImage) {
        this.isPlaceholderImage = isPlaceholderImage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
