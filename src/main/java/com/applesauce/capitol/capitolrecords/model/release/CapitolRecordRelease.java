
package com.applesauce.capitol.capitolrecords.model.release;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "version",
    "releases"
})
public class CapitolRecordRelease {

    @JsonProperty("status")
    private String status;
    @JsonProperty("version")
    private String version;
    @JsonProperty("releases")
    private Releases releases;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("releases")
    public Releases getReleases() {
        return releases;
    }

    @JsonProperty("releases")
    public void setReleases(Releases releases) {
        this.releases = releases;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
