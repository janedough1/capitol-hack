
package com.applesauce.capitol.capitolrecords.model.release;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "version",
    "type",
    "barcode",
    "year",
    "explicitContent",
    "slug",
    "artist",
    "image",
    "label",
    "licensor",
    "popularity",
    "duration",
    "trackCount",
    "adSupportedStreaming"
})
public class Release {

    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("version")
    private String version;
    @JsonProperty("type")
    private String type;
    @JsonProperty("barcode")
    private String barcode;
    @JsonProperty("year")
    private String year;
    @JsonProperty("explicitContent")
    private String explicitContent;
    @JsonProperty("slug")
    private String slug;
    @JsonProperty("artist")
    private Artist artist;
    @JsonProperty("image")
    private String image;
    @JsonProperty("label")
    private Label label;
    @JsonProperty("licensor")
    private Licensor licensor;
    @JsonProperty("popularity")
    private String popularity;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("trackCount")
    private String trackCount;
    @JsonProperty("adSupportedStreaming")
    private AdSupportedStreaming adSupportedStreaming;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("barcode")
    public String getBarcode() {
        return barcode;
    }

    @JsonProperty("barcode")
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @JsonProperty("year")
    public String getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonProperty("explicitContent")
    public String getExplicitContent() {
        return explicitContent;
    }

    @JsonProperty("explicitContent")
    public void setExplicitContent(String explicitContent) {
        this.explicitContent = explicitContent;
    }

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    @JsonProperty("slug")
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("artist")
    public Artist getArtist() {
        return artist;
    }

    @JsonProperty("artist")
    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("label")
    public Label getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(Label label) {
        this.label = label;
    }

    @JsonProperty("licensor")
    public Licensor getLicensor() {
        return licensor;
    }

    @JsonProperty("licensor")
    public void setLicensor(Licensor licensor) {
        this.licensor = licensor;
    }

    @JsonProperty("popularity")
    public String getPopularity() {
        return popularity;
    }

    @JsonProperty("popularity")
    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("trackCount")
    public String getTrackCount() {
        return trackCount;
    }

    @JsonProperty("trackCount")
    public void setTrackCount(String trackCount) {
        this.trackCount = trackCount;
    }

    @JsonProperty("adSupportedStreaming")
    public AdSupportedStreaming getAdSupportedStreaming() {
        return adSupportedStreaming;
    }

    @JsonProperty("adSupportedStreaming")
    public void setAdSupportedStreaming(AdSupportedStreaming adSupportedStreaming) {
        this.adSupportedStreaming = adSupportedStreaming;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
