
package com.applesauce.capitol.capitolrecords.model.release;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "page",
    "pageSize",
    "totalItems",
    "release"
})
public class Releases {

    @JsonProperty("page")
    private String page;
    @JsonProperty("pageSize")
    private String pageSize;
    @JsonProperty("totalItems")
    private String totalItems;
    @JsonProperty("release")
    private List<Release> release = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("page")
    public String getPage() {
        return page;
    }

    @JsonProperty("page")
    public void setPage(String page) {
        this.page = page;
    }

    @JsonProperty("pageSize")
    public String getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("totalItems")
    public String getTotalItems() {
        return totalItems;
    }

    @JsonProperty("totalItems")
    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    @JsonProperty("release")
    public List<Release> getRelease() {
        return release;
    }

    @JsonProperty("release")
    public void setRelease(List<Release> release) {
        this.release = release;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
