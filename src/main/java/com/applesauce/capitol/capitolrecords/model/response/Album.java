
package com.applesauce.capitol.capitolrecords.model.response;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "album",
    "songs"
})
public class Album {

    @JsonProperty("album")
    private String album;
    @JsonProperty("songs")
    private List<Song> songs = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("album")
    public String getAlbum() {
        return album;
    }

    @JsonProperty("album")
    public void setAlbum(String album) {
        this.album = album;
    }

    @JsonProperty("songs")
    public List<Song> getSongs() {
        return songs;
    }

    @JsonProperty("songs")
    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
