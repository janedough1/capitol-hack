
package com.applesauce.capitol.capitolrecords.model.tracks;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "version",
    "artist",
    "trackNumber",
    "duration",
    "explicitContent",
    "isrc",
    "pline",
    "type",
    "release",
    "discNumber",
    "number",
    "adSupportedStreaming"
})
public class Track {

    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("version")
    private String version;
    @JsonProperty("artist")
    private Artist artist;
    @JsonProperty("trackNumber")
    private String trackNumber;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("explicitContent")
    private String explicitContent;
    @JsonProperty("isrc")
    private String isrc;
    @JsonProperty("pline")
    private String pline;
    @JsonProperty("type")
    private String type;
    @JsonProperty("release")
    private Release release;
    @JsonProperty("discNumber")
    private String discNumber;
    @JsonProperty("number")
    private String number;
    @JsonProperty("adSupportedStreaming")
    private AdSupportedStreaming adSupportedStreaming;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("artist")
    public Artist getArtist() {
        return artist;
    }

    @JsonProperty("artist")
    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    @JsonProperty("trackNumber")
    public String getTrackNumber() {
        return trackNumber;
    }

    @JsonProperty("trackNumber")
    public void setTrackNumber(String trackNumber) {
        this.trackNumber = trackNumber;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("explicitContent")
    public String getExplicitContent() {
        return explicitContent;
    }

    @JsonProperty("explicitContent")
    public void setExplicitContent(String explicitContent) {
        this.explicitContent = explicitContent;
    }

    @JsonProperty("isrc")
    public String getIsrc() {
        return isrc;
    }

    @JsonProperty("isrc")
    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    @JsonProperty("pline")
    public String getPline() {
        return pline;
    }

    @JsonProperty("pline")
    public void setPline(String pline) {
        this.pline = pline;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("release")
    public Release getRelease() {
        return release;
    }

    @JsonProperty("release")
    public void setRelease(Release release) {
        this.release = release;
    }

    @JsonProperty("discNumber")
    public String getDiscNumber() {
        return discNumber;
    }

    @JsonProperty("discNumber")
    public void setDiscNumber(String discNumber) {
        this.discNumber = discNumber;
    }

    @JsonProperty("number")
    public String getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("adSupportedStreaming")
    public AdSupportedStreaming getAdSupportedStreaming() {
        return adSupportedStreaming;
    }

    @JsonProperty("adSupportedStreaming")
    public void setAdSupportedStreaming(AdSupportedStreaming adSupportedStreaming) {
        this.adSupportedStreaming = adSupportedStreaming;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
