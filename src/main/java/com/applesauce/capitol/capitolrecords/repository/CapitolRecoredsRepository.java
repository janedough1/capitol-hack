package com.applesauce.capitol.capitolrecords.repository;

import com.applesauce.capitol.capitolrecords.exception.CapitolRecordsException;
import com.applesauce.capitol.capitolrecords.model.artist.CapitolRecordArtist;
import com.applesauce.capitol.capitolrecords.model.release.CapitolRecordRelease;
import com.applesauce.capitol.capitolrecords.model.tracks.CapitolRecordTracks;

/**
 * Created by jan on 6/2/18.
 */
public interface CapitolRecoredsRepository {

    CapitolRecordArtist searchArtist(String name) throws CapitolRecordsException;

    CapitolRecordRelease getReleases(String id) throws CapitolRecordsException;

    CapitolRecordTracks getTracks(String artistId) throws CapitolRecordsException;
}
