package com.applesauce.capitol.capitolrecords.repository;

import com.applesauce.capitol.capitolrecords.model.setlist.Setlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jan on 6/3/18.
 */
@Repository
public interface SetListRepository extends JpaRepository<Setlist, Long> {
    List<Setlist> findBySetname(String setname);
}
