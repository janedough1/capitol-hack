package com.applesauce.capitol.capitolrecords.repository.impl;

import com.applesauce.capitol.capitolrecords.config.CapitolRecordConfig;
import com.applesauce.capitol.capitolrecords.exception.CapitolRecordsException;
import com.applesauce.capitol.capitolrecords.model.artist.CapitolRecordArtist;
import com.applesauce.capitol.capitolrecords.model.release.CapitolRecordRelease;
import com.applesauce.capitol.capitolrecords.model.tracks.CapitolRecordTracks;
import com.applesauce.capitol.capitolrecords.repository.CapitolRecoredsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

/**
 * Created by jan on 6/2/18.
 */
@Repository
public class CapitolRecoredsRepositoryImpl implements CapitolRecoredsRepository {

    private RestTemplate restTemplate;

    private final CapitolRecordConfig capitolRecordConfig;

    @Autowired
    public CapitolRecoredsRepositoryImpl(RestTemplate restTemplate, CapitolRecordConfig capitolRecordConfig) {
        this.restTemplate = restTemplate;
        this.capitolRecordConfig = capitolRecordConfig;
    }

    @Override
    public CapitolRecordArtist searchArtist(String name) throws CapitolRecordsException {

        HttpEntity<?> entity = new HttpEntity<>(createHttpHeaders());

        return restTemplate.exchange(
                capitolRecordConfig.getUrl() + "search/" + name,
                HttpMethod.GET,
                entity,
                CapitolRecordArtist.class).getBody();
    }

    @Override
    public CapitolRecordRelease getReleases(String id) throws CapitolRecordsException {
        HttpEntity<?> entity = new HttpEntity<>(createHttpHeaders());

        return restTemplate.exchange(
                capitolRecordConfig.getUrl() + "releases/" + id,
                HttpMethod.GET,
                entity,
                CapitolRecordRelease.class).getBody();
    }

    @Override
    public CapitolRecordTracks getTracks(String artistId) throws CapitolRecordsException {
        HttpEntity<?> entity = new HttpEntity<>(createHttpHeaders());

        return restTemplate.exchange(
                capitolRecordConfig.getUrl() + "tracks/" + artistId,
                HttpMethod.GET,
                entity,
                CapitolRecordTracks.class).getBody();
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }
}
