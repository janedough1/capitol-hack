package com.applesauce.capitol.capitolrecords.service;

import com.applesauce.capitol.capitolrecords.exception.CapitolRecordsException;
import com.applesauce.capitol.capitolrecords.model.artist.CapitolRecordArtist;
import com.applesauce.capitol.capitolrecords.model.release.CapitolRecordRelease;
import com.applesauce.capitol.capitolrecords.model.response.Song;

import java.util.List;

/**
 * Created by jan on 6/2/18.
 */
public interface CapitolRecordsService {

    CapitolRecordArtist searchArtist(String name) throws CapitolRecordsException;

    CapitolRecordRelease getReleases(String id) throws CapitolRecordsException;

    List<Song> getAllSongs(String artistId) throws CapitolRecordsException;
}
