package com.applesauce.capitol.capitolrecords.service;

import com.applesauce.capitol.capitolrecords.exception.SetlistException;
import com.applesauce.capitol.capitolrecords.model.setlist.Setlist;

import java.util.List;

/**
 * Created by jan on 6/3/18.
 */
public interface SetListService {

    List<Setlist> getSetList() throws SetlistException;

    List<Setlist> getSetListBySetname(String setname) throws SetlistException;

    Setlist createSetList(Setlist setlist) throws SetlistException;

    List<Setlist> createSetListList(List<Setlist> setlists) throws SetlistException;
}
