package com.applesauce.capitol.capitolrecords.service.impl;

import com.applesauce.capitol.capitolrecords.exception.CapitolRecordsException;
import com.applesauce.capitol.capitolrecords.model.artist.CapitolRecordArtist;
import com.applesauce.capitol.capitolrecords.model.release.CapitolRecordRelease;
import com.applesauce.capitol.capitolrecords.model.response.Song;
import com.applesauce.capitol.capitolrecords.model.tracks.CapitolRecordTracks;
import com.applesauce.capitol.capitolrecords.model.tracks.Track;
import com.applesauce.capitol.capitolrecords.repository.CapitolRecoredsRepository;
import com.applesauce.capitol.capitolrecords.service.CapitolRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jan on 6/2/18.
 */
@Service
public class CapitolRecordsServiceImpl implements CapitolRecordsService{

    private final CapitolRecoredsRepository capitolRecoredsRepository;

    @Autowired
    public CapitolRecordsServiceImpl(CapitolRecoredsRepository capitolRecoredsRepository) {
        this.capitolRecoredsRepository = capitolRecoredsRepository;
    }


    @Override
    public CapitolRecordArtist searchArtist(String name) throws CapitolRecordsException {
        return capitolRecoredsRepository.searchArtist(name);
    }

    @Override
    public CapitolRecordRelease getReleases(String id) throws CapitolRecordsException {
        return capitolRecoredsRepository.getReleases(id);
    }

    @Override
    public List<Song> getAllSongs(String artistId) throws CapitolRecordsException {

        List<Song> songNames = new ArrayList<>();
        CapitolRecordTracks capitolRecordTracks = capitolRecoredsRepository.getTracks(artistId);
        for (Track track : capitolRecordTracks.getTracks().getTrack()) {
            if(!songNames.stream().anyMatch(t -> t.getName().equals(track.getTitle()))) {
                Song song = new Song();
                song.setName(track.getTitle());
                songNames.add(song);
            }
        }

        return songNames;
    }


}
