package com.applesauce.capitol.capitolrecords.service.impl;

import com.applesauce.capitol.capitolrecords.exception.SetlistException;
import com.applesauce.capitol.capitolrecords.model.setlist.Setlist;
import com.applesauce.capitol.capitolrecords.repository.SetListRepository;
import com.applesauce.capitol.capitolrecords.service.SetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jan on 6/3/18.
 */
@Service
public class SetListServiceImpl implements SetListService {

    private final SetListRepository setListRepository;

    @Autowired
    public SetListServiceImpl(SetListRepository setListRepository) {
        this.setListRepository = setListRepository;
    }

    @Override
    public List<Setlist> getSetList() throws SetlistException {
        return setListRepository.findAll();
    }

    @Override
    public List<Setlist> getSetListBySetname(String setname) throws SetlistException {
        return setListRepository.findBySetname(setname);
    }

    @Override
    public Setlist createSetList(Setlist setlist) throws SetlistException {

        return setListRepository.save(setlist);
    }

    @Override
    public List<Setlist> createSetListList(List<Setlist> setlists) throws SetlistException {

        for(Setlist setlist : setlists) {
            setListRepository.save(setlist);
        }

        return setlists;
    }


}
